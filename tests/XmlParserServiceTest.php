<?php
namespace Test;

use App\Service\XmlParserService;
use App\Storage\FileStorage;
use PHPUnit\Framework\TestCase;

final class XmlParserServiceTest extends TestCase
{
    public function testIfParseThrowsAnException(): void
    {
        $parser = new XmlParserService(new FileStorage('test_output.csv'));
        $this->expectException(\Exception::class);
        $parser->parse('');
    }

    public function testParse(): void
    {
        $parser = new XmlParserService(new FileStorage('test_output.csv'));
        $this->assertTrue($parser->parse('https://www.w3schools.com/xml/plant_catalog.xml'));
    }
}