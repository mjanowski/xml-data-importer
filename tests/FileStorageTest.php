<?php
namespace Test;

use App\Storage\FileStorage;
use PHPUnit\Framework\TestCase;

final class FileStorageTest extends TestCase
{
    public function testSetColumns(): void
    {
        $fileStorage = new FileStorage('test.csv');
        $cols = ['col1', 'col2', 'col3'];
        $fileStorage->setColumns($cols);
        $this->assertSame($cols, $fileStorage->getColumns());
    }

    public function testGetRows(): void
    {
        $fileStorage = new FileStorage('test.csv');
        $rows = [
            ['XYZ', 123, 'Lorem Ipsum', 0, true],
            ['Lorem ipsum', 0, 'Dolor amet', 123, false],
            ['This is an example text', 234, 'Another random text', 999, true]
        ];
        foreach ($rows as $row) {
            $fileStorage->addRow($row);
        }
        $this->assertSame($rows, $fileStorage->getRows());
    }

    public function testIfPersistThrowsAnException(): void
    {
        $fileStorage = new FileStorage('');
        $this->expectException(\Exception::class);
        $fileStorage->persist();
    }


}