FROM php:cli-alpine

WORKDIR /app

ENTRYPOINT [ "/bin/sh", "-c" ]
CMD ["php app.php","app:parse-data"]


