# XML Data Importer

Parse XML file and export as a CSV file
## Prerequirements
To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

## Build & run containers

Build & run containers `docker-compose up`


## Usage
Parse local file:
`docker-compose run app 'php ./app.php app:parse-data sourceFile.xml outputFile.csv'`

Parse file from URL:
`docker-compose run app 'php ./app.php app:parse-data https://www.w3schools.com/xml/plant_catalog.xml outputFile.csv'`

## Tests
Run unit tests: `docker-compose run app 'vendor/bin/phpunit --verbose tests'
`
