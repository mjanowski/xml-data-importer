<?php
namespace App\Service;

use App\Storage\StorageInterface;

class XmlParserService implements ParserInterface
{
    protected StorageInterface $storage;

    public function __construct(StorageInterface $storage) {
        $this->storage = $storage;
    }

    public function parse(string $source): bool
    {
        if ($source != '') {
            $file = file_get_contents($source);

            if ($file) {
                $xmlData = new \SimpleXMLElement($file);

                foreach ($xmlData as $item) {
                    $this->storage->setColumns(array_keys(get_object_vars($item)));
                    $row = [];
                    foreach ($item as $key => $value) {
                        $row[$key] = (string)$value;
                    }
                    $this->storage->addRow($row);
                }
                return $this->storage->persist();
            }
        }

        throw new \Exception('Cannot read file');
    }

}