<?php
namespace App\Storage;

interface StorageInterface
{
    public function setColumns(array $columns): void;
    public function getColumns(): array;
    public function addRow(array $row): void;
    public function getRows(): array;
    public function persist(): bool;
}