<?php
namespace App\Storage;

class FileStorage implements StorageInterface
{
    protected array $columns = [];
    protected array $rows = [];
    protected string $output;

    public function __construct(string $output)
    {
        $this->output = $output;
    }

    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function addRow(array $row): void
    {
        $this->rows[] = $row;
    }

    public function getRows(): array
    {
        return $this->rows;
    }

    public function persist(): bool
    {
        $path = getcwd() . '/' . $this->output;

        if (!is_dir($path)) {
            $file = fopen($path, 'w');

            if ($file !== false) {
                fputcsv($file, $this->columns);
                foreach ($this->rows as $row) {
                    fputcsv($file, $row);
                }
            }
            return true;
        }

        throw new \Exception('Error opening the file ' . $path);
    }

}