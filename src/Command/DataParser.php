<?php
namespace App\Command;

use App\Service\XmlParserService;
use App\Storage\FileStorage;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

#[AsCommand(
    name: 'app:parse-data',
    description: 'Parse XML data and export CSV'
)]
class DataParser extends Command
{
    protected static $defaultName = 'app:parse-data';
    protected LoggerInterface $logger;

    public function __construct(string $name = null)
    {
        parent::__construct($name);
        $this->logger = new Logger('logger');
        $this->logger->pushHandler(new StreamHandler(getcwd() . '/var/log/app.log', Level::Debug));
    }

    protected function configure(): void
    {
        $this->addArgument('sourceFile', InputArgument::REQUIRED, 'Path to the source file');
        $this->addArgument('outputFile', InputArgument::REQUIRED, 'Path to the output file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'XML Parser',
            '============',
            '',
        ]);
        $sourceFile = $input->getArgument('sourceFile');
        $outputFile = $input->getArgument('outputFile');

        $sourceFile = filter_var($sourceFile, FILTER_VALIDATE_URL) ? $sourceFile : (getcwd() . '/' . $sourceFile);

        try {
            $parser = new XmlParserService(new FileStorage($outputFile));
            $parser->parse($sourceFile);

        } catch (\Throwable $exception) {
            $this->logger->error($exception->getMessage());
            $output->writeln('An error occurred. Please check logs file to see the details.');
            return Command::FAILURE;
        }
        $output->writeln('Result successfully saved in ' . $outputFile);
        return Command::SUCCESS;
    }
}